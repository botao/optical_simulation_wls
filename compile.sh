#!/bin/bash

SCRIPT_DIR=$(cd $(dirname $0); pwd)

source $SCRIPT_DIR/setenv.sh
cmake $SCRIPT_DIR
make -j4
